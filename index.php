<?php
include('includes/config.php');
?>

<?php
$msg = "";
if (isset($_POST['submit'])) { 
        
        $name = $_POST['name']; 
        $fname = $_POST['fathersName'];
        $mName = $_POST['mothersName'];
        $grandfName = $_POST['grandFatherName'];    
        $maritalStatus = $_POST['maritalStatus'];//
        $educationalStatus = $_POST['educationalStatus'];
        $gender = $_POST['gender'];  //  
        $perAddress = $_POST['permanentAddress'];
        $preAddress = $_POST['presentAddress'];
        $cast = $_POST['cast'];    
        $guardianNumber = $_POST['guardianNumber'];
        $mobileNumber = $_POST['mobileNumber'];
        $lifePartnerSelf = $_POST['lifePaertnerSelf'];    
        $yourSelf = $_POST['yourSelf'];
        $birthDate = $_POST['birthday'];
        $profession = $_POST['profession'];
        $religion = $_POST['religion'];    
        $facebookId = $_POST['facebookId'];
       
        $image = $_FILES["image"]["name"];
?>


<?php
  
//// Code for move image into directory
        move_uploaded_file($_FILES["image"]["tmp_name"], "admin/userImage/" . $image);

        

        
        $query=mysqli_query($con,"insert into user(name,fathersName,mothersName,grandFname,guardianNumber,gender,cast,religion,maritalStatus,permanentAddress,presentAddress,image,facebookId,educationalStatus,birthDate,profession,yourself,lifePartnerSelf,mobileNumber) values('$name','$fname','$mName','$grandfName','$guardianNumber','$gender','$cast','$religion','$maritalStatus','$perAddress','$preAddress','$image','$facebookId','$educationalStatus','$birthDate','$profession','$yourSelf','$lifePartnerSelf','$mobileNumber')");
        if($query) {
            $msg = "ধন্যবাদ সফলভাবে রেজিস্ট্রেশন সম্পন্ন হয়েছে।";
        }
 else {
     $msg = "দুঃখিত , রেজিস্ট্রেশন সম্পন্ন হয়নি।";
 }
    }


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Colorlib Templates">
        <meta name="author" content="Colorlib">
        <meta name="keywords" content="Colorlib Templates">

        <title>Marriage Guardian</title>

        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

        <link href="css/main.css" rel="stylesheet" media="all">
        <style>
            .footer {
    border-top: 1px solid rgba(152, 166, 173, 0.2);
    bottom: 0;
    text-align: left !important;
    padding: 19px 30px 20px;
    position: absolute;
    right: 0;
    left: 225px;
}

        </style>



    </head>
    <body>
        <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
            <div class="wrapper wrapper--w680">
                <div class="card card-1">
                    <div class="card-heading"></div>
                    <div class="card-body">
                        <h4 class="title">নিজের পছন্দ মতো লাইফ পার্টনার খুজতে ফ্রি রেজিস্ট্রেশন করুন ।
বিঃদ্রঃ আমাদের কাছে ১০ হাজারের বেশি পাত্র পাত্রী আছে।
</h4>
                        <div>
                            <h5 style="color: green"><?php echo $msg; ?></h5>
                        </div>
                        
                        
                        <form method="post" enctype="multipart/form-data">
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="আপনার নাম :" name="name" required>
                            </div>
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="আপনার পিতার নাম :" name="fathersName" required>
                            </div>
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="আপনার মাতার নাম :" name="mothersName" required>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="আপনার দাদার নাম(ঐচ্ছিক) :" name="grandFatherName">
                            </div>
                               </div>
                                <div>
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="অভিভাবকের ফোন  নাম্বার :" name="guardianNumber" >
                            </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="বর্তমান ঠিকানা :" name="presentAddress" required="">
                            </div>
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="স্থায়ী ঠিকানা  :" name="permanentAddress">
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <input class="input--style-1 js-datepicker" type="text" placeholder="জন্মতারিখ" name="birthday" required="">
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="gender" required="">
                                                <option disabled="disabled" selected="selected">লিঙ্গ</option>
                                                <option>নারী</option>
                                                <option>পুরুষ</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="maritalStatus" required="">
                                                <option disabled="disabled" selected="selected">বৈবাহিক অবস্থা</option>
                                                <option>ডিভোর্সি</option>
                                                <option>অবিবাহিত</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <input class="input--style-1" type="text" placeholder="আপনার ফোন  নাম্বার :" name="mobileNumber" >
                            </div>
                                </div>
                                
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="পেশা :" name="profession">
                            </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <input class="input--style-1" type="text" placeholder="ধর্ম :" name="religion" >
                            </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="বংশ/গোত্র (ঐচ্ছিক) :" name="cast">
                            </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="ফেসবুক আইডি (ঐচ্ছিক) :" name="facebookId">
                            </div>
                                </div>
                            </div>

                            
                                    <div class="input-group">
                                        <label for="image">আপনার ছবি যুক্ত করুন :</label>
                                        <input class="input--style-1" type="file" accept="image/x-png,image/jpg,image/jpeg" id="image" name="image" required="">
                            </div>
                     
                               
                                    <div class="input-group">
                                        <input class="input--style-1" type="text" placeholder=" শিক্ষাগত যোগ্যতা  " name="educationalStatus">
                            </div>
                                
                            
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="আপনি কেমন লাইফ পার্টনার পছন্দ করেন ? তার সম্পর্কে কিছু লিখুন  " name="lifePaertnerSelf">
                            </div>
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="আপনার সম্পর্কে কিছু লিখুন  " name="yourSelf">
                            </div>
                            
                            <div class="p-t-20">
                                <input class="btn btn--radius btn--green" name="submit" type="submit" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <footer style="text-align: center;color: white">
                <br><br><br><br><br><br>
                2020 © Developed by<a style="color:greenyellow ;text-decoration: none;" href="https://samimhossain.xyz"> Md. Samim Hossain</a>
                </footer>
        </div>
        


        <script src="vendor/jquery/jquery.min.js" type="text/javascript"></script>

        <script src="vendor/select2/select2.min.js" type="text/javascript"></script>
        <script src="vendor/datepicker/moment.min.js" type="text/javascript"></script>
        <script src="vendor/datepicker/daterangepicker.js" type="text/javascript"></script>

        <script src="js/global.js" type="text/javascript"></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="text/javascript"></script>
        <script type="text/javascript">
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-23581568-13');
        </script>
        <script src="../../../../ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="d0e60a8e99b0a45f0300ab55-|49" defer=""></script></body>

    <!-- Mirrored from colorlib.com/etc/regform/colorlib-regform-1/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jun 2020 17:22:26 GMT -->
</html>

