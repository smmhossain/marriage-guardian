<?php 
session_start();
include('includes/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Marriage Guardian</title>

        <!--Morris Chart CSS -->
		<link rel="stylesheet" href="../plugins/morris/morris.css">

        <!-- jvectormap -->
        <link href="../plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../plugins/switchery/switchery.min.css">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

        

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <?php include('includes/topheader.php');?>

            <!-- ========== Left Sidebar Start ========== -->
           <?php include('includes/leftsidebar.php');?>


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Manage Posts </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Admin</a>
                                        </li>
                                        <li>
                                            <a href="#">Posts</a>
                                        </li>
                                        <li class="active">
                                            Manage Post  
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->




                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    

 <?php
 $id = $_GET['uid'];
$query=mysqli_query($con,"select * from user where id='$id'");
while($row=mysqli_fetch_array($query))
{
    ?><div class="row" style="padding-left: 30px;">
        
        <div class="col-sm-4">
            <img src="<?php echo "userImage/".$row['image']; ?>" alt="" style="height: 200px; width: 250px;">
            
        </div>
        
        <div class="col-sm-4">
            <h5><b>Name :</b><?php echo htmlentities($row['name']);?></h5>
            <h5><b>Father's Name :</b><?php echo htmlentities($row['fathersName']);?></h5>
            <h5><b>Mother's Name :</b><?php echo htmlentities($row['mothersName']);?></h5>
            <h5><b>Grand father's Name :</b><?php echo htmlentities($row['grandFname']);?></h5>           
            <h5><b>Guardian number :</b><?php echo htmlentities($row['guardianNumber']);?></h5>
            <h5><b>Gender :</b><?php echo htmlentities($row['gender']);?></h5>
    </div>
        <div class="col-sm-4">
            <h5><b>Marital Status :</b><?php echo htmlentities($row['maritalStatus']);?></h5>
            <h5><b>Birth Date :</b><?php echo htmlentities($row['birthDate']);?></h5>
            <h5><b>profession :</b><?php echo htmlentities($row['profession']);?></h5>
            <h5><b>Religion :</b><?php echo htmlentities($row['religion']);?></h5>
            <h5><b>Cast :</b><?php echo htmlentities($row['cast']);?></h5>
            <h5><b>Mobile number :</b><?php echo htmlentities($row['mobileNumber']);?></h5>
                                    
                                
    </div>
        </div>
        <div style="padding-top: 30px;padding-left: 30px;">
                                    <h5><b>Educational Status :</b><?php echo htmlentities($row['educationalStatus']);?></h5>
                                    <h5><b>Facebook Id link :</b><?php echo htmlentities($row['facebookId']);?></h5>
                                    <h5><b>Permanent Address :</b><?php echo htmlentities($row['permanentAddress']);?></h5>
                                    <h5><b>present Address :</b><?php echo htmlentities($row['presentAddress']);?></h5>
                                    <h5><b>User's Details :</b><?php echo htmlentities($row['yourself']);?></h5>
                                    <h5><b>User's choose :</b><?php echo htmlentities($row['lifePartnerSelf']);?></h5>
        </div>
        
<?php } ?>
                                
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->

       <?php include('includes/footer.php');?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


       
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <!-- CounterUp  -->
        <script src="../plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="../plugins/counterup/jquery.counterup.min.js"></script>

        <!--Morris Chart-->
		<script src="../plugins/morris/morris.min.js"></script>
		<script src="../plugins/raphael/raphael-min.js"></script>

        <!-- Load page level scripts-->
        <script src="../plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../plugins/jvectormap/gdp-data.js"></script>
        <script src="../plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script>


        <!-- Dashboard Init js -->
		<script src="assets/pages/jquery.blog-dashboard.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        
        <script charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready( function () {
            $('#postTable').DataTable();
        } );
        </script>

    </body>
</html>
<?php } ?>